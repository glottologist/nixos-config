{ config, pkgs, ... }:

{
  imports =
    [
      /etc/nixos/hardware-configuration.nix
      ../../profiles/hardware.nix
      ../../modules/amd
      ../../modules/nvidia
      ../../profiles/desktop.nix
      ../../profiles/personal.nix
      ../../profiles/workstation.nix
      ../../profiles/communication.nix
      ../../profiles/gaming.nix
    ];

  boot.loader.grub.device = "/dev/sda";

  # no access time and continuous TRIM for SSD
  fileSystems."/".options = [ "noatime" "discard" ];

  hardware.cpu.intel.updateMicrocode = true;
  # use latest kernel to have best performance
  boot.kernelPackages = pkgs.linuxPackages_latest;

  #boot.extraModulePackages = [ pkgs.linuxPackages_latest.v4l2loopback ];
  #boot.kernelModules = [ "v4l2loopback" ];
  #boot.extraModprobeConfig = ''
  #  options v4l2loopback exclusive_caps=1 video_nr=9 card_label="OBS"
  #'';

  hardware.gpu.amd.enable = true;
  hardware.gpu.nvidia.enable = false;

  # prevent ping drop by increasing ARP table
  # neighbour: arp_cache: neighbor table overflow!
  boot.kernel.sysctl = {
    "net.ipv4.neigh.default.gc_thresh1" = 32768;
    "net.ipv4.neigh.default.gc_thresh2" = 65536;
    "net.ipv4.neigh.default.gc_thresh3" = 131072;
    # The number 4,294,967,295, equivalent to the hexadecimal value FFFF,FFFF16, is the maximum value for a 32-bit unsigned integer in computing.
    # https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
    # https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/tree/net/core/neighbour.c?h=v5.7.19#n2326
  };

  networking = {
    hostName = "gaming";
    domain = "lan";

    firewall.enable = false;
  };

  # install packages
  environment.systemPackages = with pkgs; [
    klavaro
  ];

  # compatible NixOS release
  system.stateVersion = "19.03";
}
