{ config, pkgs, lib, ... }:

{
  imports =
    [
      /etc/nixos/hardware-configuration.nix
      ../../profiles/hardware.nix
      ../../profiles/server.nix
      ../../profiles/desktop.nix
      ../../profiles/personal.nix
      #../../services/drone-runner
      #../../services/boinc.nix
    ];

  boot.loader.grub.device = "/dev/sda";

  # no access time and continuous TRIM for SSD
  fileSystems."/".options = [ "noatime" "discard" ];

  networking = rec {
    # hostname from mnemonic encoding word list
    # http://web.archive.org/web/20091003023412/http://tothink.com/mnemonic/wordlist.txt
    hostName = "zipper";
    domain = "lan";

    interfaces = {
      enp4s0.ipv4.addresses = [
        { address = "10.0.0.52"; prefixLength = 8; }
      ];
    };

    nameservers = [ "10.0.0.1" ];
    defaultGateway = { address = "10.0.0.1"; interface = "enp4s0"; };

    firewall = {
      enable = false;
      allowPing = true;
      allowedTCPPorts = [ 80 443 19999 ];
      allowedUDPPorts = [];
    };

    useDHCP = false;
  };

  services.fail2ban.enable = false;
  services.syncthing.enable = false;

  # not used
  documentation.man.enable = false;

  services.xserver.enable = false;
  services.xserver.displayManager.lightdm.enable = false;
  services.xserver.desktopManager.pantheon.enable = false;

  services.xrdp.enable = true;
  services.xrdp.defaultWindowManager = "${pkgs.xfce4-14.xfce4-session}/bin/xfce4-session";
  services.xserver.desktopManager.xfce.enable = true;

  # hypervisor virtualization
  virtualisation.virtualbox.host.enable = true;

  # Monitoring
  services.netdata = {
    enable = true;
    config = {
      global = {
        "default port" = "19999";
        "bind to" = "*";
        "memory mode" = "dbengine";
        "page cache size" = "64";
        "dbengine disk space" = "512";
        "error log" = "syslog";
        "debug log" = "syslog";
      };
    };
  };
  services.vnstat.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}
