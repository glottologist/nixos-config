# NixOS Server

for the NixOS Survey 2020

## Services

- [LimeSurvey](https://www.limesurvey.org/en) (Apache + PHP-FPM)
- Monitoring with [netdata](https://my-netdata.io/) and [vnStat](http://humdi.net/vnstat/)
- Backup of Database and files using [restic](https://restic.net/)
