{ config, pkgs, lib, ... }:

{
  imports =
    [
      /etc/nixos/hardware-configuration.nix
      ../../profiles/server.nix
      ../../services/backup
    ];

  boot.loader.grub.device = "/dev/sda";

  networking = rec {
    hostName = "survey";
    domain = "davidak.de";

    interfaces = {
      ens3.ipv4.addresses = [
        { address = "78.47.220.153"; prefixLength = 32; }
      ];
      ens3.ipv6.addresses = [
        { address = "2a01:4f8:c0c:6e2c::1"; prefixLength = 64; }
      ];
    };

    nameservers = [ "213.133.98.98" "213.133.99.99" "213.133.100.100" ];
    defaultGateway = { address = "172.31.1.1"; interface = "ens3"; };

    firewall = {
      enable = true;
      allowPing = true;
      allowedTCPPorts = [ 80 443 19999 ];
      allowedUDPPorts = [];
    };

    useDHCP = false;
  };

  services.limesurvey = {
    enable = true;
    virtualHost = {
      hostName = "survey.nixos.org";
      adminAddr = "post@davidak.de";
      enableACME = true;
      forceSSL = true;
    };
  };

  security.acme = {
    email = "post@davidak.de";
    acceptTerms = true;
  };

  services.mysqlBackup = {
    enable = true;
    databases = [ "mysql" "limesurvey" ];
    user = "root";
    calendar = "03:30";
    singleTransaction = true;
  };

  # Monitoring
  services.netdata = {
    enable = true;
    config = {
      global = {
        "default port" = "19999";
        "bind to" = "*";
        "memory mode" = "dbengine";
        "page cache size" = "128";
        "dbengine disk space" = "1792"; # 14 days x 128 MiB
        "error log" = "syslog";
        "debug log" = "syslog";
      };
    };
  };
  services.vnstat.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}
