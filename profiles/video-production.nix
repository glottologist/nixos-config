{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  # install packages
  environment.systemPackages = with pkgs; [
    audacity
    # wait for 1.0 release; use flatpak RC until then
    #unstable.pitivi
    kdenlive
  ];
}
