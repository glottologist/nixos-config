{ config, pkgs, lib, ... }:

{
  imports =
    [
      ../services/grub.nix
      ../services/ssh.nix
      ../services/ntp.nix
      ../services/dns.nix
      ../services/nix.nix
      ../services/localization.nix
    ];

  # mount tmpfs on /tmp
  boot.tmpOnTmpfs = lib.mkDefault true;

  # show IP in login screen
  # https://github.com/NixOS/nixpkgs/issues/63322
  environment.etc."issue.d/ip.issue".text = "\\4\n";
  networking.dhcpcd.runHook = "${pkgs.utillinux}/bin/agetty --reload";

  # install basic packages
  environment.systemPackages = with pkgs; [
    htop
    iotop
    iftop
    wget
    curl
    tcpdump
    telnet
    whois
    siege
    file
    lsof
    inotify-tools
    strace
    gdb
    xz
    lz4
    zip
    unzip
    rsync
    restic
    micro
    xclip
    tealdeer
    screen
    tree
    dfc
    pwgen
    jq
    yq
    gitAndTools.gitFull
  ];

  programs.bash.enableCompletion = true;

  environment.variables = {
    "EDITOR" = "micro";
    "VISUAL" = "micro";
  };

  # don't install documentation i don't use
  documentation.enable = true; # documentation of packages
  documentation.nixos.enable = false; # nixos documentation
  documentation.man.enable = true; # manual pages and the man command
  documentation.info.enable = false; # info pages and the info command
  documentation.doc.enable = false; # documentation distributed in packages' /share/doc

  # copy the system configuration into nix-store
  system.copySystemConfiguration = true;
}
