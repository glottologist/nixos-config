{ config, pkgs, lib, ... }:

let
  unstable = import <nixos-unstable> {};
  inherit (lib) optionals;
in
{
  imports =
    [
      ./common.nix
      # TODO: simplify
      #../services/avahi.nix
      ../services/avahi-server.nix
      ../services/avahi-client.nix
      ../services/fonts
      ../users/davidak/base.nix
      ../modules/disable-internet-at-night
    ];

  # boot splash instead of log messages
  boot.plymouth.enable = true;

  # use elementarys pantheon desktop environment
  services.xserver.enable = lib.mkDefault true;
  services.xserver.useGlamor = true;
  services.xserver.displayManager.lightdm.enable = lib.mkDefault true;
  services.xserver.desktopManager.pantheon.enable = lib.mkDefault true;

  # disable xterm session
  services.xserver.desktopManager.xterm.enable = false;

  # enable flatpak support
  #services.flatpak.enable = true;
  #xdg.portal.enable = true;

  # enable fwupd, a DBus service that allows applications to update firmware
  #services.fwupd.enable = true;

  hardware.keyboard.zsa.enable = true;

  programs.ssh.startAgent = true;

  disable-internet-at-night.enable = lib.mkDefault true;

  programs.chromium = {
    enable = true;
    extensions = [
      "cbnipbdpgcncaghphljjicfgmkonflee" # Axel Springer Blocker
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
      "mnjggcdmjocbbbhaepdhchncahnbgone" # SponsorBlock for YouTube
      "hjdoplcnndgiblooccencgcggcoihigg" # Terms of Service; Didn’t Read
      "gcbommkclmclpchllfjekcdonpmejbdp" # HTTPS Everywhere
      "oboonakemofpalcgghocfoadofidjkkk" # KeePassXC-Browser
      "fploionmjgeclbkemipmkogoaohcdbig" # Page load time
      "feeoiklfggbaibpdhkkngbpkppdmcjal" # Tab Counter
      "iflpdolpbldahbddinmkobondnhpikim" # Mein Grundeinkommen - CrowdBar
    ];
    extraOpts = {
      "BrowserSignin" = 0;
      "SyncDisabled" = true;
      "PasswordManagerEnabled" = false;
      "AutofillAddressEnabled" = true;
      "AutofillCreditCardEnabled" = false;
      "BuiltInDnsClientEnabled" = false;
      "MetricsReportingEnabled" = true;
      "SearchSuggestEnabled" = false;
      "AlternateErrorPagesEnabled" = false;
      "UrlKeyedAnonymizedDataCollectionEnabled" = false;
      "SpellcheckEnabled" = true;
      "SpellcheckLanguage" = [
                               "de"
                               "en-US"
                             ];
      "CloudPrintSubmitEnabled" = false;
    };
  };

  # install packages
  environment.systemPackages = with pkgs; [
    atom
    apostrophe
    meld
    chromium
    firefox
    keepassx-community
    #libreoffice
    gnumeric
    gimp
    todo-txt-cli
    python3Packages.xkcdpass
    python3Packages.youtube-dl
    remmina
    gnome3.gnome-disk-utility
    gnome3.gnome-system-monitor
    appimage-run
  ] ++ optionals config.services.boinc.enable [ boinc ];
}
