{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  imports =
    [
      <home-manager/nixos>
    ];

  home-manager.users.davidak = { pkgs, config, ... }: {
    # set sessionVariables also for X session
    home.file.".xprofile".text = ''. "${config.home.profileDirectory}/etc/profile.d/hm-session-vars.sh"'';
    home.sessionVariables = {
      # enable MangoHud for Vulkan games
      MANGOHUD = 1;
    };
  };

  # allow some unfree packages
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam"
    "steam-original"
    "steam-runtime"
  ];

  programs.steam.enable = true;
  hardware.steam-hardware.enable = true;

  # install packages
  environment.systemPackages = with pkgs; [
    mumble
    mangohud
    unstable.goverlay

    # games
    #crawl
    #multimc
  ];
}
