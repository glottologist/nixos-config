{ config, lib, pkgs, ... }:

let
  inherit (lib) mkOption mkEnableOption mkIf types;
  cfg = config.disable-internet-at-night;
in
{
  options.disable-internet-at-night = {
    enable = mkEnableOption "internet-free night time";

    timestart = mkOption {
      type = types.str;
      default = "22:00";
      example = "23:00";
      description = ''
        Configured when to start the internet free time (Hour:Minute:Second).
      '';
    };

    timestop = mkOption {
      type = types.str;
      default = "8:00";
      example = "6:00";
      description = ''
        Configured when to stop the internet free time (Hour:Minute:Second).
      '';
    };
  };

  config = mkIf cfg.enable {
    systemd = {
      timers.disable-internet = {
        description = "Disable internet timer";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = cfg.timestart;
          Unit = "disable-internet.service";
          Persistent = "true";
        };
      };

      services.disable-internet = {
        description = "Disable internet service";
        enable = true;
        script = ''
          ${pkgs.iptables}/bin/iptables -w -I OUTPUT ! --dst 10.0.0.0/8 -j DROP
        '';
      };

      timers.enable-internet = {
        description = "Enable internet timer";
        wantedBy = [ "timers.target" ];
        timerConfig = {
          OnCalendar = cfg.timestop;
          Unit = "enable-internet.service";
          Persistent = "true";
        };
      };

      services.enable-internet = {
        description = "Enable internet service";
        enable = true;
        script = ''
          ${pkgs.iptables}/bin/iptables -w -D OUTPUT ! --dst 10.0.0.0/8 -j DROP
        '';
      };
    };

    meta = with lib; {
      maintainers = with maintainers; [ davidak ];
    };
  };
}
