# disable-internet-at-night

NixOS module to disable internet access at night (from 10 pm to 8 am).

The local network (10.0.0.0/8) is still accessible.

## Usage

1. Make sure your firewall is enabled.

```
networking.firewall.enable = true;
```

2. Import the module.

```
imports =
  [
    ../modules/disable-internet-at-night
  ];
```

3. Enable the option.

```
disable-internet-at-night.enable = true;
```

You can also specify the start and end time.

```
disable-internet-at-night = {
  enable = true;
  timestart = "23:00";
  timestop = "6:00";
}
```

## References

Commands from https://gist.github.com/waldyrious/948a6f0ddee13aed07de32f86b617af4
