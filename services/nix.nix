{ config, ... }:

{
  nix = {
    enable = true;
    useSandbox = true;
    trustedUsers = [
      "root"
      "davidak"
    ];
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
}
