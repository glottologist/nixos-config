{ config, ... }:

let
  secrets = import ./secrets.nix;
in
{
  virtualisation.oci-containers.containers = {
    drone-runner = {
      image = "drone/drone-runner-docker:1";
      ports = [ "3000:3000" ];
      volumes = [ "/var/run/docker.sock:/var/run/docker.sock" ];
      environment = {
        DRONE_RUNNER_NAME = config.networking.hostName;
        DRONE_RUNNER_CAPACITY = "${toString config.nix.maxJobs}";
        DRONE_RPC_HOST = "ci.gutesoftware.de";
        DRONE_RPC_SECRET = secrets.DRONE_RPC_SECRET;
        DRONE_RPC_PROTO = "https";
      };
    };
  };
}
