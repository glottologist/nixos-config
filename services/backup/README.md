# Backup Service

Setup restic backup service.

## Usage

Add to machines `configuration.nix`:

```
imports =
  [
    ../../services/backup
  ];
```

Create file `/etc/nixos/restic-minio` with content:

```
AWS_ACCESS_KEY_ID=**********
AWS_SECRET_ACCESS_KEY=**********
```

Create file `/etc/nixos/restic-password` with content:

```
**********
```

Replace with the actual password.

If the machine is in the local network, use the internal domain:

```
services.restic.backups.nas = {
  repository = "s3:http://nas.lan:9000/restic/restic";
};
```
