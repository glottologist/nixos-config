{ config, lib, ... }:

{
  services.restic.backups.nas = {
    repository = lib.mkDefault "s3:https://lan.davidak.de:9001/restic/restic";
    s3CredentialsFile = "/etc/nixos/restic-minio";
    passwordFile = "/etc/nixos/restic-password";
    paths = lib.mkDefault [ "/etc/nixos/" "/etc/passwd" "/etc/shadow" "/root" "/home" "/var/lib" "/var/www" "/var/backup/" ];
    extraBackupArgs = [ "--exclude=/home/*/.local/share/Steam/steamapps/common" ];
    timerConfig = lib.mkDefault { OnCalendar = "04:00"; RandomizedDelaySec = "2h"; Persistent = "true"; };
  };
}
