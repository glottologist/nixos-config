{ config, pkgs, lib, ... }:

let
  email = "david.kleuker@greenbone.net";
in
{
  imports =
    [
      <home-manager/nixos>
    ];

  home-manager.users.davidak = { pkgs, ... }: {
    #home.packages = with pkgs; [ fortune ];

    programs.git = {
      userName  = "David Kleuker";
      userEmail = email;
    };
  };
}
